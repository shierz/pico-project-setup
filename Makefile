default: build

bin-path := ~/bin
package-name := gitlab.com/shierz/pico-project-setup
app-name := pico-project-setup

.PHONY: build

build:
	go build -v -o $(bin-path)/$(app-name)
