# Pico-Project-Setup

This tool should help setting starting up a new Raspberry Pi Pico project structure.

Currently, it can create a new folder, that can then be opened in CLion and work can be started

## Quickstart

### Build

#### Requirements

* pico-sdk and toolchain (picotool, openocd, picoprobe) is installed and working (see Pico C/C++ tutorial https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf)
* golang
* make
* folder `~/bin` exists and is in `$PATH`
* env var `$PICO_SDK_PATH` is set or path is provided via flag

#### build

```
$ make
```

#### execute for a CLion Pico project

`pico-project-setup pico-clion ${PROJECT-NAME}`

this command does the following:
* create a new directory `${PROJECT-NAME}`
* create structure as CLion would for a new project
* copy files from SDK
* write `CMakeLists.txt`
* write `openocd.cfg` (for the debug target, so we can build and flash via picoprobe from CLion)
* write configs in `.idea`