package pico_clion

import (
	"bytes"
	"embed"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
)

//go:embed templates/*
var templates embed.FS

var gitignore = `.idea
.tools
cmake-build-debug
`

type Project struct {
	root    string
	Name    string
	SdkPath string
}

func NewProject(root string, name string, sdkPath string) *Project {
	return &Project{
		root:    root,
		Name:    name,
		SdkPath: sdkPath,
	}
}

func (p *Project) InitFolder() (err error) {

	err = os.Mkdir(p.root+string(os.PathSeparator)+"build", 0755)
	if err != nil {
		return
	}

	err = os.Mkdir(p.root+string(os.PathSeparator)+".idea", 0755)
	if err != nil {
		return
	}

	err = os.Mkdir(p.root+string(os.PathSeparator)+".tools", 0755)
	if err != nil {
		return
	}

	err = os.Mkdir(p.root+string(os.PathSeparator)+"cmake-build-debug", 0755)
	if err != nil {
		return
	}

	return
}

func (p *Project) AddFilesToDir() (err error) {

	pdir := []string{}
	idea := []string{".idea"}
	tools := []string{".tools"}

	err = p.writeFileFromTemplate(pdir, "main.c", "main.c")
	if err != nil {
		return
	}

	err = p.writeFileFromTemplate(pdir, "CMakeLists.txt", "CMakeLists.txt")
	if err != nil {
		return
	}

	err = p.writeFileFromTemplate(tools, "openocd.cfg", "openocd.cfg")
	if err != nil {
		return
	}

	err = p.writeFileFromTemplate(idea, "modules.xml", "modules.xml")
	if err != nil {
		return
	}

	err = p.writeFileFromTemplate(idea, "misc.xml", "misc.xml")
	if err != nil {
		return
	}

	err = p.writeFileFromTemplate(idea, "workspace.xml", "workspace.xml")
	if err != nil {
		return
	}

	err = p.writeFileFromTemplate(idea, p.Name+".iml", "project.iml")
	if err != nil {
		return
	}

	err = p.writeFile(idea, ".name", p.Name)
	if err != nil {
		return
	}

	err = p.writeFile(pdir, ".gitignore", gitignore)
	if err != nil {
		return
	}

	err = p.copyFromSdk(pdir, []string{"external"}, "pico_sdk_import.cmake")
	if err != nil {
		return
	}

	return
}

func (p *Project) writeFileFromTemplate(folders []string, filename, tpl string) (err error) {
	var b bytes.Buffer
	var t *template.Template
	t, err = template.ParseFS(templates, "templates"+string(os.PathSeparator)+tpl)
	if err != nil {
		return
	}
	err = t.Execute(&b, p)
	if err != nil {
		return
	}
	return p.writeFile(folders, filename, b.String())
}

func (p *Project) writeFile(folders []string, filename, content string) (err error) {
	ps := string(os.PathSeparator)
	filename = p.root + ps + strings.Join(folders, ps) + ps + filename
	return ioutil.WriteFile(filename, []byte(content), 0644)
}

func (p *Project) copyFromSdk(foldersTarget, foldersSdk []string, filename string) (err error) {
	var input []byte
	ps := string(os.PathSeparator)

	inputFilename := p.SdkPath + ps + strings.Join(foldersSdk, ps) + ps + filename
	outputFilename := p.root + ps + strings.Join(foldersTarget, ps) + ps + filename

	input, err = ioutil.ReadFile(inputFilename)
	return ioutil.WriteFile(outputFilename, input, 0644)
}
