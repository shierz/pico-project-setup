/*
Copyright © 2022 shierz <cqpt@cqpt.eu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"errors"
	"github.com/spf13/cobra"
	pico_clion "gitlab.com/shierz/pico-project-setup/pico-clion"
	"os"
	"strings"
)

// picoClionCmd represents the picoClion command
var picoClionCmd = &cobra.Command{
	Use:   "pico-clion",
	Short: "setup directory for new C/C++ pico project with CLion",
	Long:  `setup directory for new C/C++ pico project with CLion`,
	RunE: func(cmd *cobra.Command, args []string) (err error) {
		if len(args) < 1 || strings.TrimSpace(args[0]) == "" {
			return errors.New("no project-name passed")
		}
		name := args[0]
		folder, err := cmd.Flags().GetString("folder")
		if err != nil {
			return
		}
		if folder == "" {
			folder = name
		}
		sdkPath, err := cmd.Flags().GetString("sdk-path")
		if err != nil {
			return
		}
		if sdkPath == "" {
			var ok bool
			if sdkPath, ok = os.LookupEnv("PICO_SDK_PATH"); !ok {
				return errors.New("no SDK PATH passed")
			}
		}

		err = os.Mkdir(folder, 0755)
		if err != nil {
			return
		}

		project := pico_clion.NewProject(folder, name, sdkPath)

		err = project.InitFolder()
		if err != nil {
			return
		}

		err = project.AddFilesToDir()
		if err != nil {
			return
		}

		return
	},
}

func init() {
	rootCmd.AddCommand(picoClionCmd)

	picoClionCmd.Flags().StringP("sdk-path", "p", "", "path to C/C++ Pico-SDK")
	picoClionCmd.Flags().StringP("folder", "f", "", "provide the folder-name, if it differs from ./PROJECT-NAME")
}
